#include "stack.h"
#include<assert.h>


//=============================================================================
/* creates an empty list */
Stack initializeStack(void)
{
    return createEmptyList();
}
//=============================================================================
/* returns 1 if stack is empty, 0 otherwise */
int stackIsEmpty(const Stack stack)
{
    return isEmpty(stack);
}
//=============================================================================
/* add last */
void push(Stack* stack, const Data data)
{
    addLast(stack, data);
    assert(data == getLastElement(*stack));   
}
//=============================================================================
/* remove last */
void pop(Stack* stack)
{
    assert(isEmpty(*stack) != 1);

    removeLast(stack);
}
//=============================================================================
/* checks last element */
Data peekStack(const Stack stack)
{
    return getLastElement(stack);
}
//=============================================================================
/* prints the stack */
void printStack(const Stack stack, FILE *textFile)
{
    printList(stack, textFile);
}
