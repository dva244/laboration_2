#include "queue.h"
#include<assert.h>


//=============================================================================
/* creates an empty list */
/* why not use createEmptyList? */
Queue initializeQueue(void)
{
    return NULL;
}
//=============================================================================
/* returns 1 if it is empty, 0 otherwise*/
int queueIsEmpty(const Queue queue)
{
    return isEmpty(queue);
}
//=============================================================================
/* addLast */
void enqueue(Queue* queue, const Data data)
{
    addLast(queue, data);

    assert(getLastElement(*queue) == data);
}
//=============================================================================
/* removeFirst */
void dequeue(Queue* queue)
{
    assert(*queue != NULL);

    removeFirst(queue);
}
//=============================================================================
/* checks first element */
Data peekQueue(const Queue queue)
{
    assert(queue != NULL);

    return getFirstElement(queue);
}
//=============================================================================
/* prints the queue */
void printQueue(const Queue queue, FILE *textfile)
{
    printList(queue, textfile);    
}
//=============================================================================
/* rotates queue, back goes to front */
void rotate(Queue* queue, int steps)
{
    assert(steps >= 0);

    int i = 0;

    if(isEmpty(*queue) == 1)
    {
        return;
    }

    while(i < steps)
    {
        enqueue(queue, peekQueue(*queue));
        dequeue(queue);
        i++;
    }
}
//=============================================================================
int dequeueDuplicates(Queue* queue)
{
    assert(isEmpty(*queue) != 1);

    int removed = 0;
    Queue temp = initializeQueue();
    Queue newq = initializeQueue();


    enqueue(&newq, peekQueue(*queue));

    while(peekQueue(newq) == peekQueue(*queue))
    {
        enqueue(&temp, peekQueue(*queue));
        dequeue(queue);
        removed++;
    }

    //incase first was removed but there are no subsequent similar numbers after it, then we have to re add it again
    if (removed == 1)
    {
        while(isEmpty(*queue) != 1)
        {
            enqueue(&temp, peekQueue(*queue));
            dequeue(queue);
        }
        while(isEmpty(temp) != 1)
        {
            enqueue(queue, peekQueue(temp));
            dequeue(&temp);
        }
        return 0;
    }

    assert((peekQueue(newq) != peekQueue(*queue)) || (isEmpty(*queue) == 1));
    return(removed);
}
