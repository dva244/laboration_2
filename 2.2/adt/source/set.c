#include "set.h"
#include <assert.h>


//=============================================================================
/* creates an empty list */
Set initializeSet(void)
{
    return createEmptyList();
}
//=============================================================================
/* adds first if data is not in set already */
void addToSet(Set* set, const Data element)
{
    if (isInSet(*set, element) == 0)
    {
        addFirst(set, element);

        assert(isInSet(*set, element) != 0);
    }
}
//=============================================================================
/* removes data from set */
void removeFromSet(Set* set, const Data element)
{
    removeElement(set, element);

    assert(isInSet(*set, element) == 0);
}
//=============================================================================
/* checks if data is in the set */
int isInSet(const Set set, const Data element)
{
    return search(set, element);
}
//=============================================================================
/* prints the set */
void printSet(const Set set, FILE *textfile)
{
    printList(set, textfile);
}
