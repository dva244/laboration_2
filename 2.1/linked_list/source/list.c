#include "list.h"
#include <stdlib.h>
#include <assert.h>


//=============================================================================
/*Hjalpfunktion till add*/
static struct node* createListNode(const Data data)
{
    struct node *newnode = (struct node*)malloc(sizeof(struct node));

    if (newnode != NULL)
    {
        (*newnode).data = data;
        (*newnode).next = NULL;
        return newnode;
    }
    return NULL;
}
//=============================================================================
/*Returnera en tom lista*/
List createEmptyList(void)
{
    return NULL;
}
//=============================================================================
/* Returns 1 if list is empty, 0 if it's not empty*/
int isEmpty(const List list)
{
    if (list == NULL)
    {
        return 1;
    }
    return 0;
}
//=============================================================================
/*Lagg till nod forst i listan*/
void addFirst(List *list, const Data data)
{
    struct node *newnode = createListNode(data);

    if (newnode != NULL)
    {
        (*newnode).next = *list;
        *list = newnode;
        assert(*list == newnode);
    }
}
//=============================================================================
/*Lagg till nod sist i listan*/
void addLast(List *list, const Data data)
{
    struct node *temp = *list;

    if (*list == NULL)    
    {
        addFirst(list, data);
    }
    else
    {
        while((*temp).next != NULL)
        {
            temp = (*temp).next;
        }
        addFirst(&(*temp).next, data);
    }
}
//=============================================================================
/*Ta bort forsta noden i listan*/
void removeFirst(List *list)
{
    assert(*list != NULL);

    struct node *remove = *list;

    *list = (**list).next;
    free(remove);
}
//=============================================================================
/*Ta bort sista noden i listan*/
void removeLast(List *list)
{
    assert(*list != NULL);

    struct node *remove = *list;    
    struct node *removebefore = remove;

    if ((**list).next == NULL)
    {
        removeFirst(list);
    }
    else
    {
        while((*remove).next != NULL)
        {
            removebefore = remove;
            remove = (*remove).next;
        }
        (*removebefore).next = NULL;
        removeFirst(&remove);
    }
}
//=============================================================================
/*Ta bort data ur listan (forsta forekomsten), returnernar 1 om det lyckades tas bort, 0 annars*/
int removeElement(List *list, const Data data)
{
    struct node *remove = *list;
    struct node *removebefore = remove;

    if(remove == NULL)
    {
        return 0;
    }

    while((*remove).data != data)
    {
        removebefore = remove;
        remove = (*remove).next;

        if(remove == NULL)
        {
            return 0;
        }
    }

    if (remove == *list)
    {
        removeFirst(list);
        return 1;
    }

    (*removebefore).next = (*remove).next;
    free(remove);
    return 1;
}
//=============================================================================
/*Finns data i listan? returnerar 1 om datat finns, annars 0*/
int search(const List list, const Data data)
{
    struct node *temp = list;

    if (list == NULL)
    {
        return 0;
    }
    else
    {
        while((*temp).data != data)
        {
            temp = (*temp).next;
            if(temp == NULL)
            {
                return 0;
            }
        }
        return 1;
    }
}
//=============================================================================
/* räknar noderna i listan */
int numberOfNodesInList(const List list)
{
    struct node *temp = list;
    int i = 0;

    while (temp != NULL)
    {
        temp = (*temp).next;
        i++;
    }
    return i;
}
//=============================================================================
/*Ta bort alla noder ur listan*/
void clearList(List *list)
{
    while (*list != NULL)
    {
        removeLast(list);
    }
    assert(*list == NULL);
}
//=============================================================================
/*Skriv ut listan*/
void printList(const List list, FILE *textfile)
{
    struct node *temp = list;

    while (temp != NULL)
    {
        fprintf(textfile, "Data: %d\nNext: %p\nAdress: %p\n", (*temp).data, (void*)(*temp).next, (void*)temp);
        temp = (*temp).next;
    }
}
//=============================================================================
/*Returnera forsta datat i listan*/
Data getFirstElement(const List list)
{
    assert(list != NULL);

    return (*list).data;
}
//=============================================================================
/*Returnera sista datat i listan*/
Data getLastElement(const List list)
{
    assert(list != NULL);

    struct node *temp = list;

    while((*temp).next != NULL)
    {
        temp = (*temp).next;
    }
    return (*temp).data;
}
